data "aws_ami" "latest-ubuntu" {
  most_recent = true
  owners = ["099720109477"]

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-18.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "this" {
  key_name = "${local.app}-key"
  public_key = var.public_key
}

resource "aws_instance" "this" {
  ami = data.aws_ami.latest-ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.this.key_name
  vpc_security_group_ids = [ aws_security_group.ec2.id ]

  tags = {
    Name = "${local.app}-ec2"
  }
}

resource "aws_security_group" "ec2" {
  name = "${local.app}-ec2-sg"

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 22
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${local.app}-ec2-sg"
  }
}

resource "aws_eip" "this" {
  instance = aws_instance.this.id
  vpc = true

  tags = {
    Name = "${local.app}-eip"
  }
}

output "ec2_ip" {
  value = aws_eip.this.public_ip
}
