provider "aws" {
  region = "ap-southeast-2"
}

locals {
  app = "dd-cars-api-${var.env}"
}

