# Provisioning

## Prerequisites

* You need to have AWS account and credentials setup
* You also need to have terraform installed

To deploy infrastructure for UAT, run command

```
./deploy.sh
```

To deploy infrastructure for Production, run command

```
./deploy.sh production
```

## TODO

Have a proper pipeline for infrastructure deployment
