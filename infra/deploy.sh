#!/bin/bash

export "$(cat .env | xargs)"

ENV=uat
case "$1" in
  "production")
    ENV=production;;
esac

cd terraform

echo "${PUBLIC_KEY}"

terraform workspace select ${ENV} || terraform workspace new ${ENV}

terraform init \
  -var env="${ENV}" \
  -var public_key="${PUBLIC_KEY}" \
  -input=false

terraform apply \
  -var env="${ENV}" \
  -var public_key="${PUBLIC_KEY}" \
  -auto-approve

ip=$(terraform output ec2_ip | tr -d '"')

cd ..

ssh -o StrictHostKeyChecking=no ubuntu@$ip << 'ENDSSH'
sudo apt update -y
sudo apt install nginx -y
sudo apt install docker.io -y
sudo usermod -aG docker ubuntu
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
ENDSSH

scp -o StrictHostKeyChecking=no -r ./nginx/default.conf ubuntu@$ip:/home/ubuntu

ssh -o StrictHostKeyChecking=no ubuntu@$ip << ENDSSH
sudo mv default.conf /etc/nginx/sites-enabled/default
sudo systemctl restart nginx
ENDSSH
