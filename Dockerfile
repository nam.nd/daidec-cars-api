FROM node:14-alpine

WORKDIR /app

COPY . .

RUN npm ci

EXPOSE 4000

CMD ["npm", "start"]
