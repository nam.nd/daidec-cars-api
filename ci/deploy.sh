#!/bin/sh

apk add --no-cache openssh-client
mkdir -p ~/.ssh
echo "$EC2_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
scp -o StrictHostKeyChecking=no -r ./.env ./docker-compose.prod.yml ubuntu@$EC2_PUBLIC_IP_ADDRESS:/home/ubuntu

ssh -o StrictHostKeyChecking=no ubuntu@$EC2_PUBLIC_IP_ADDRESS << 'ENDSSH'
    cd /home/ubuntu
    export $(cat .env | xargs)
    docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    docker pull $IMAGE
    docker-compose -f docker-compose.prod.yml up -d
ENDSSH

