import { CarSearchResult, QueryCarsArgs } from '../types.generated'
import { CarsContext } from './types'

const resolvers = {
  Query: {
    cars: async (
      _parent: unknown,
    { params, pagination }: QueryCarsArgs,
    { dataSources }: CarsContext,
    ): Promise<CarSearchResult> => dataSources.carsAPI.search(params, pagination ?? undefined)
  }
}

export default resolvers
