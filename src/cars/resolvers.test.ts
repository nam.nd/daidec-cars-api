import { CarSearchResult, QueryCarsArgs } from '../types.generated'
import { CarsAPI } from './datasource'
import resolvers from './resolvers'

describe('cars resolvers', () => {
  describe('cars query', () => {
    const dataSources = {
      carsAPI: new CarsAPI()
    }
    const mockArgs = {
      params: {
        name: 'car name'
      }
    } as QueryCarsArgs
    const mockResult = {
      totalCount: 1
    } as CarSearchResult

    let stub: jest.SpyInstance
    beforeEach(() => {
      stub = jest.spyOn(dataSources.carsAPI, 'search').mockResolvedValue(mockResult)
    })

    it('searches without pagination', async () => {
      await resolvers.Query.cars(null, mockArgs, { dataSources })

      expect(stub).toHaveBeenCalledWith(mockArgs.params, undefined)
    })

    it('searches with pagination', async () => {
      const mockArgsWithPagination = {
        ...mockArgs,
        pagination: {
          currentPage: 2,
          pageSize: 5
        }
      } as QueryCarsArgs

      await resolvers.Query.cars(null, mockArgsWithPagination, { dataSources })

      expect(stub).toHaveBeenCalledWith(mockArgs.params, mockArgsWithPagination.pagination)
    })

    it('returns the result', async () => {
      const result = await resolvers.Query.cars(null, mockArgs, { dataSources })

      expect(result).toEqual(mockResult)
  })
  })
})
