import { readFileSync } from 'fs'
import { RawData } from './types'

export const fetch = (): RawData[] => {
  try {
    const seedFile = './seeders/cars.json'
    const rawData = readFileSync(seedFile, 'utf8')
    const result = JSON.parse(rawData.toString())
    return result as RawData[]
  } catch (error) {
    // TODO: We proably do not want to throw this error to the consumer
    // However, it's good idea to log this error as part of system monitoring
    console.log(error)
    return []
  }
}
