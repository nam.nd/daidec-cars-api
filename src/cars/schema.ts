import { gql } from 'apollo-server'

export default gql`

  scalar Date

  input Pagination {
    """
    Default value: 1
    """
    currentPage: Int
    """
    Default value: 10
    """
    pageSize: Int
  }

  input CarSearchParams {
    name: String!
    """
    Default value: false
    """
    exactMatch: Boolean
  }

  type Car {
    name: String!
    milesPerGallon: Float
    cylinders: Int
    displacement: Int
    horsepower: Int
    weightInLbs: Int
    acceleration: Float
    """
    Manufacture date in format YYYY-MM-DD
    """
    year: Date
    """
    Origin country
    """
    origin: String
  }

  type PageInfo {
    hasNextPage: Boolean!
    currentPage: Int!
    numberOfRecords: Int!
  }

  type CarSearchResult {
    totalCount: Int!
    pageInfo: PageInfo!
    records: [Car!]!
  }

  type Query {
    cars(params: CarSearchParams!, pagination: Pagination): CarSearchResult!
  }
`

