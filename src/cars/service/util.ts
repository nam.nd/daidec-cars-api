import { Car } from '../../types.generated'
import { RawData } from '../repository/types'

export const mapRawToCar = (raw: RawData): Car => ({
  name: raw.Name ?? '',
  milesPerGallon: raw.Miles_per_Gallon,
  cylinders: raw.Cylinders,
  displacement: raw.Displacement,
  horsepower: raw.Horsepower,
  weightInLbs: raw.Weight_in_lbs,
  acceleration: raw.Acceleration,
  year: raw.Year,
  origin: raw.Origin,
})

