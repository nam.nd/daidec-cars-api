import { Car } from '../../types.generated'
import * as carsRepository from '../repository'
import { mapRawToCar } from './util'

export const fetchCars = (): Car[] => {
  const rawData = carsRepository.fetch()
  return rawData.map(mapRawToCar)
}
