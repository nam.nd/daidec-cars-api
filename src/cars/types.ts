import { CarsAPI } from './datasource'

type CarsDataSource = {
  carsAPI: CarsAPI
}
export type CarsContext = {
  dataSources: CarsDataSource
}
