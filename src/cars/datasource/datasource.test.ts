import { CarsAPI } from '.'

describe('cars datasource', () => {
  describe('searches exact match of name', () => {
    const carsAPI = new CarsAPI()
    carsAPI['_cars'] = [
      {
        name: 'one',
      },
      {
        name: 'two'
      },
      {
        name: 'another one',
      },
      {
        name: 'another two',
      },
      {
        name: 'third two'
      }
    ]


    it('returns only cars with exact name matched', async () => {
      const results = await carsAPI.search({
        name: 'one',
        exactMatch: true
      })

      expect(results.totalCount).toEqual(1)
    })

    it('returns fuzzy match name', async () => {
      const results = await carsAPI.search({
        name: 'one',
      })

      expect(results.totalCount).toEqual(2)
    })

    it('returns page that is not the last one', async () => {
      const results = await carsAPI.search({
        name: 'two',
      }, {
        pageSize: 2,
      })

      expect(results.pageInfo.hasNextPage).toEqual(true)
    })

    it('returns last page result', async () => {
      const results = await carsAPI.search({
        name: 'two',
      }, {
        pageSize: 2,
        currentPage: 2,
      })

      expect(results.pageInfo.hasNextPage).toEqual(false)
    })
  })
})
