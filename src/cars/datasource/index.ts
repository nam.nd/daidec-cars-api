import { DataSource } from 'apollo-datasource'
import fuzzysort from 'fuzzysort'
import { Car, CarSearchParams, CarSearchResult, Pagination } from '../../types.generated'
import * as carsService from '../service'

const DEFAULT_PAGE_SIZE = 10

export class CarsAPI extends DataSource {

  private _cars: Car[]

  constructor() {
    super()
    this._cars = []
    this.loadData()
  }

  private loadData = () => {
    this._cars = carsService.fetchCars()
  }

  search = async (params: CarSearchParams, pagination?: Pagination): Promise<CarSearchResult> => {
    const { name, exactMatch = false } = params
    const results = exactMatch ? this.searchByName(name) : this.fuzzySearchByName(name)

    const pageSize = pagination?.pageSize ?? DEFAULT_PAGE_SIZE
    const currentPage = pagination?.currentPage ?? 1
    const startIndex = (currentPage - 1) * pageSize
    const endIndex = currentPage * pageSize
    const records = results.slice(startIndex, endIndex)

    return {
      totalCount: results.length,
      records,
      pageInfo: {
        hasNextPage: endIndex < results.length,
        currentPage,
        numberOfRecords: records.length
      }
    }
  }

  private searchByName = (name: string): Car[] => {
    return this._cars.filter((c: Car) => c.name === name)
  }

  private fuzzySearchByName = (name: string): Car[] => {
    return fuzzysort.go(name, this._cars, { key: 'name' }).map((r: Fuzzysort.KeyResult<Car>) => r.obj)
  }
}
