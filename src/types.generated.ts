// THIS FILE IS AUTO-GENERATED, DO NOT EDIT!
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
};

export type Car = {
  __typename?: 'Car';
  name: Scalars['String'];
  milesPerGallon?: Maybe<Scalars['Float']>;
  cylinders?: Maybe<Scalars['Int']>;
  displacement?: Maybe<Scalars['Int']>;
  horsepower?: Maybe<Scalars['Int']>;
  weightInLbs?: Maybe<Scalars['Int']>;
  acceleration?: Maybe<Scalars['Float']>;
  /** Manufacture date in format YYYY-MM-DD */
  year?: Maybe<Scalars['Date']>;
  /** Origin country */
  origin?: Maybe<Scalars['String']>;
};

export type CarSearchParams = {
  name: Scalars['String'];
  /** Default value: false */
  exactMatch?: Maybe<Scalars['Boolean']>;
};

export type CarSearchResult = {
  __typename?: 'CarSearchResult';
  totalCount: Scalars['Int'];
  pageInfo: PageInfo;
  records: Array<Car>;
};


export type PageInfo = {
  __typename?: 'PageInfo';
  hasNextPage: Scalars['Boolean'];
  currentPage: Scalars['Int'];
  numberOfRecords: Scalars['Int'];
};

export type Pagination = {
  /** Default value: 1 */
  currentPage?: Maybe<Scalars['Int']>;
  /** Default value: 10 */
  pageSize?: Maybe<Scalars['Int']>;
};

export type Query = {
  __typename?: 'Query';
  cars: CarSearchResult;
};


export type QueryCarsArgs = {
  params: CarSearchParams;
  pagination?: Maybe<Pagination>;
};
