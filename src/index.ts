import { ApolloServer } from 'apollo-server'
import typeDefs from './cars/schema'
import resolvers from './cars/resolvers'
import { CarsAPI } from './cars/datasource'

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    carsAPI: new CarsAPI()
  })
})

server.listen().then(({ url }) => {
  console.log(`GraphQL server running at ${url}`)
})
