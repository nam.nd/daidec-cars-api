module.exports = {
  clearMocks: true,
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  testMatch: [
    '**/?(*.)+(spec|test).ts',
  ],
}

