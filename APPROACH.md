# Brainstorming

The problem we want to solve is quite popular and there are a handful of options that can accomplish the task.

* The simplest option is probably just using a search services from some of the cloud providers. For example, we can just dump the whole dataset into AWS Elasticsearch or Azure Cognitive Search service, the data will then be automatically exposed by the services's REST API. Both of these two services supports indexing and querying (even fuzzy search) that allows us to perform simple search by name or advanced search by field keyword, which is exactly what the API after. However, the disadvantage of this approach is that we do not have full control over how the REST API works. For example, if in the future we want to have our own authentication mechanism in front of the API, this approach won't work or if the cloud provider change their API behaviour, we also must inform our API consumers about the change, and so on.

* The other option (which is quite trendy) is to use a combination of cloud services, such as AWS API Gateway + Lambda function, or Google Firebase Cloud Functions + Cloud Firestore. This is a very scalable approach, and work really well for some use cases, especially event driven, or asynchronous behaviour applications. However, there are also some downside with this approach. We will be locked in with the cloud provider. If we want to move to different cloud provider, we will have to redo everything almost from scratch. We can use Serverless framework which abstract away cloud provider's APIs to avoid that. Yet, there is still a common issue in the serverless world, that is the "cold start" issue. There are ways to keep the services "warm", or even "hot" all the time, but that would also come with with an extra cost, in some case would be even more expensive then traditional servers. Another big disadvantage of serverless applications in my opinion is that it is not very dev-friendly in terms of writing unit testing, and debugging.

* The other common approach is to build a simple two tier architecture, where the API act as the client application and the database act as the server. With this approach, you have full control of the API, where you want to deploy it to, and of course you can also design the infrastructure to be high performing, scalable, and highly available.

# Decision

Out of the above approaches, I am leaning towards to the last one, which is using the two tier architecture with traditional server infrastructure.

For the client application tier, there are two most popular ones nowsaday, REST API or GraphQL. Eventhough REST API would do the job just fine, I am in favour of GraphQL for a couple of reasons:

1. The requirement could extends to support query by other parameters such as year range, horsepower, origin, etc. While filtering is possible in REST API, it is not as tidy and readable as GraphQL.
1. Pagination is a must-have when dealing with querying data. Eventhough, in our use case, the number of cars in the fleet might not be a big number, we still definitely do not want overfetching. Again, this can be done with REST API too, but it looks much more neat in GraphQL
1. Last but not least, GraphQL has built-in schema and documentation (and a playground too), that make it is super dev friendly. And since the users of our API is probably most (if not all) are engineers, it's a pretty important reason in my opinion.

Regarding the database, there are also plenty of choices. Both SQL (PostgreSQL, MySQL) and NoSQL (MongoDB) does support standard query, and text search that will surely satisfy the requirement. However, with the simple given json file, I will just use file system and abstract the main logic so that if we need to switch to a full fledge database storage engine like PostgreSQL, it would be a simple task.

# Tools & Framework

* My language of choice is NodeJS/TypeScript
* Apollo Server for GraphQL API implementation
* Docker / Docker Compose for local dev as well as for CI CD
* GitLab CI/CD
* Terraform for IaC
* AWS/EC2 for infrastructure
* Nginx for reverse proxy server

It might sound a bit overkilled for such a simple task. However, I think a production-quality API that is dev-friendly, maintainable and extendable should be well crafted from day 1.

# Improvement

## Infrastructure

* Right now the API is deployed to a simple AWS EC2 instance in a public subnet which is not a good practice.
A proper production infra should include a proper VPC with multi available zones, where the application get deployed to an EC2 instance in private subnets and exposed to the internet via Internet Gateway and Application Load Balancer.
* The EC2 instance is open SSH to the internet (for the sake of simple and free GitLab CICD). This need to be fixed for production as well.
* We do have the infrastructure as code, however, it would be better if we have a pipeline for it as well.
* Nginx should be configured to run on SSL Certificate


## Deployment

* UAT environment is a stable environment for other external stakeholders to test the API. It would be nice to have a DEV environment for internal engineers to test out before deploying to UAT.
