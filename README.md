# Daidec Cars API

See [this](./APPROACH.md) for more context

Demo is available [here](http://3.24.131.195/)

## Local development

### Using docker

You will need to have Docker and Docker Compose installed in your host machine

From the root directory of project, run the command to spin up API docker container

```
docker-compose up
```

### Without docker

You will need to have nodejs v14+ installed in your host machine

From the root directory of project, run the command to start the API app

```
npm start
```

### Playground

The playground is accessible at http://localhost:4000


## Infrastructure

See [this](./infra/README.md) for more information

## Deployment

* Any commit pushed will trigger build and test stages
* Any commit merged to master will be automatically deployed to UAT.
* Deployment to production requires approval input
